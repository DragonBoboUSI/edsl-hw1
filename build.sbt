val scala3Version = "3.2.0"

lazy val root = project
  .in(file("."))
  .settings(
    developers := List(Developer("firstname.lastname", "FirstName LastName", "firstanme.lastname@usi.ch", url("https://yourwebsite.com"))),
    name := "edsl-assignment-01-template",
    version := "1.0.0",
    scalaVersion := scala3Version,
  )
